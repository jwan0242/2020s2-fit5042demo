package helloworld;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Session Bean implementation class NameStorageBean
 */
@Singleton
@LocalBean
public class NameStorageBean {

	/**
	 * Default constructor.
	 */
	public NameStorageBean() {
		// TODO Auto-generated constructor stub
	}

	private String name = "World";
	
	private String price ="0";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	

}
